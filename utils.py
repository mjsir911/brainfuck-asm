#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

import sys
import functools

__appname__     = "utils"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

warn = functools.partial(print, "WARNING: ", file=sys.stderr)
debug = functools.partial(print, file=sys.stderr)


def strCounter(fmt):
    i = 0
    while True:
        yield fmt.format(i)
        i += 1
