#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

from utils import *

__appname__     = "jump"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

id_origpointer_dict = {}

original_order = []

id_map = {}

newTmpId = strCounter("_TMP_ID_{}")

LOOP_UNROLL_VAR = "$LOOP_UNROLLER"

class JumpManager():
    jump_stack = []
    id_replacement_map = {}

    def __init__(self, command, test_ptr, id_):
        self.id_ = id_
        self.test_ptr = test_ptr
        self.forwards = command == "JMPFNZ"

    def first(self):
        if self.forwards:
            yield "JMPFNZF {}".format(self.test_ptr)
        else:
            yield "ANCHORB {}".format(self.test_ptr)

        self.jump_stack.append(self)

    def last(self):
        myindex = self.jump_stack.index(self)
        do_after = False
        if len(self.jump_stack[myindex:]) > 1:
            prev_jumper = self.jump_stack[myindex:][-1]
            yield from prev_jumper.last()

            yield from self.last()

            new_jumper = type(self)("JMPFNZ", LOOP_UNROLL_VAR, next(newTmpId))
            yield "A_AND_NOT_B {} {} {}".format(prev_jumper.test_ptr,
                                                self.test_ptr,
                                                new_jumper.test_ptr)
            yield from new_jumper.first()
            self.id_replacement_map[prev_jumper.id_] = new_jumper.id_

            return

        if self.forwards:
            yield "ANCHORF {}".format(self.test_ptr)
        else:
            yield "JMPFNZB {}".format(self.test_ptr)

        del self.jump_stack[myindex]


    def __repr__(self):
        return "Jumper(forwards: {}, {}, {})".format(self.forwards, self.test_ptr, self.id_)

    def __eq__(self, other):
        return self.id_ == other

    def __hash__(self):
        return hash(self.id_)

    @classmethod
    def doit(cls, command, test_ptr, id_):
        id_ = cls.id_replacement_map.get(id_, id_)
        if id_ not in cls.jump_stack:
            new = cls(command, test_ptr, id_)
            print(new)
            yield from new.first()
        else:
            existing = cls.jump_stack[cls.jump_stack.index(id_)]
            yield from existing.last()


def pass_(c):
    for line in c:
        command, *args = line.split()
        if command == "JMPFNZ" or command == "ANCHOR":
            test_ptr, id_ = args
            yield from JumpManager.doit(command, test_ptr, id_)
            continue
        yield line


if __name__ == "__main__":
    import doctest
    doctest.testmod()
