#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

import re
from fileinput import input

__appname__     = "test"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""


class Code():
    code_map = {}

    def __init__(self, body):
        a = re.match(r"\s*{(.*)}", body, re.MULTILINE | re.DOTALL)
        self.body = [l.strip() for l in a[1].split(";")]
        self.body = list(filter(bool, self.body))

    @classmethod
    def uh(cls, body):
        a = cls(body)
        cls.code_map[str(a)] = a
        return str(a)

    def __str__(self):
        return "${}{:X}".format(type(self).__name__.upper(), abs(hash(tuple(self.body))))


class Function():
    func_map = {}
    def __init__(self, line, scope):
        # TODO: parse rettype
        r = re.match(r"int (\w+)\s*\(\)", line)
        self.name = r[1]
        self.scope = scope

        r = re.findall(r'(\$CODE[0-9A-F]+)', line)
        assert len(r) == 1
        self.body = Code.code_map[r[0]]

    @classmethod
    def uh(cls, *args):
        a = cls(*args)
        cls.func_map[a.name] = a

    def __call__(self):
        for line in self.body.body:
            parseLine(line, self.scope)


class Scope(dict):
    def findUnused(self):
        for i in range(10000):
            if i not in self.values():
                return i

    def alloc(self, var):
        self[var] = self.findUnused()
        return self[var]


def parseLine(line, scope):
    if line.startswith("char") or line.startswith("int"):
        if "$CODE" not in line:
            _, var = line.split()
            return print("REG ${} {}".format(var, scope.alloc(var)))
        elif ") $CODE" in line:
            return Function.uh(line, scope)

    function_call = re.match(r"(\w+)(\(.*\))", line)
    if function_call:
        a = function_call
        _, name, args = a[0], a[1], a[2]
        return Function.func_map[name]()

    assignment = re.match(r"(\w+) = (\w+)", line)
    if assignment:
        a = assignment
        var, val = a[1], a[2]
        return print("SET ${} {}".format(var, val))
    print("hi", line)


if __name__ == "__main__":
    a = "".join(input())
    scopes = re.findall(r"\s*{.*}", a, re.MULTILINE | re.DOTALL)
    for scope in scopes:
        a = a.replace(scope, " " + Code.uh(scope) + ";")

    a = a.split(";")
    a = [l.strip() for l in a]
    a = list(filter(bool, a))

    global_scope = Scope({"$R0": 0, "$JMP": 1, "$R1": 2, "$R2": 3, "$R3": 4, "$R4": 5, "$R5": 6})

    for line in a:
        parseLine(line, global_scope)

    parseLine("main()", global_scope)
