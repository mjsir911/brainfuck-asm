#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

import sys
import fileinput
from functools import partial

__appname__     = "assemble"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""

if "--debug" in sys.argv or "-d" in sys.argv:
    debug = partial(print, file=sys.stderr)
    del sys.argv[sys.argv.index("--debug")]
else:
    debug = lambda *_: None


class Allocator(dict):
    def __call__(self, variable, location=None, iterator=None):
        self[variable] = int(location) if location is not None else None
        return "ZERO {}".format(variable)

    def getUnused(self):
        for i in range(0, 10000):
            if i in self.values():
                continue
            return i

    def allocPass(self, b):
        for index, line in enumerate(b):
            if line.startswith("ptr"):
                command, *args = line.split()
                b[index] = line.replace(args[0], str(self.alloc(args[0])))
        return b

    @staticmethod
    def gotoPass(b):
        for index, line in enumerate(b):
            if line.startswith("ptr"):
                command, *args = line.split()
                b[index] = Pointer()(int(args[0]))
        return b

    def alloc(self, var):
        if self[var] is None:
            self[var] = self.getUnused()
        return self[var]


allocate = Allocator()


class TempAllocator(set):
    def __init__(self, mainAlloc):
        self.mainAlloc = mainAlloc

    def __call__(self, fmt="{}"):
        a = self.getUnused()
        self.add(a)
        var = fmt.format(a)
        self.mainAlloc(var)
        return var

    def getUnused(self):
        for i in range(0, 10000):
            if i in self:
                continue
            return i

    def tempAllocPass(self, b):
        d = {}
        for w in b.split():
            if w.startswith("$tmp"):
                if w not in d:
                    d[w] = self("${}tmp")

        for key, val in d.items():
            b = b.replace(key, val)
        return b


tempalloc = TempAllocator(allocate)


class Definition():
    def __init__(self, topline, iterator):
        _, self.command, *self.argnames = topline.split()

        b = []
        while True:
            line = next(iterator).lower().strip()
            if line == "enddef":
                break
            b.append(line)
        self.body = "\n".join(b)

    def __call__(self, *args, iterator=None):
        assert len(args) == len(self.argnames), (args, self.argnames, self.command)
        body = self.body.split("\n")
        for argname, arg in zip(self.argnames, args):
            for index, line in enumerate(body):
                body[index] = line.replace(argname, arg)

        prog = "\n".join(body)

        if "$tmp" in prog:
            prog = tempalloc.tempAllocPass(prog)

        return prog


class BlockDefinition(Definition):
    def __call__(self, *args, iterator=None):
        body = super().__call__(*args, iterator=iterator).split("\n")

        prog = []
        while True:
            line = next(iterator).lower().strip()
            if line == 'end' + self.command:
                break
            prog.append(line)

        prog = "\n".join(prog)

        body = "\n".join(l.replace("%prog", prog) for l in body).split("\n")

        return "\n".join(body)


DEFINITIONS = {}


@partial(DEFINITIONS.__setitem__, "set")
def _(location, value, iterator=None):
    if not value.isdigit():
        value = ord(value)
    ret = []
    ret.append("zero {}".format(location))
    ret.append("ptr {}".format(location))

    ret.append("+" * int(value))
    return "\n".join(ret)


class Pointer():
    current_pointer = 0

    def __call__(self, i_location, iterator=None):
        offset = (i_location - type(self).current_pointer)
        type(self).current_pointer = i_location
        if offset > 0:
            return ">" * offset
        elif offset < 0:
            return "<" * abs(offset)
        else:
            return ""
        return b




DEFINITIONS["var"] = allocate


def parseDefs(cl):
    a = iter(cl)
    newcl = []
    for line in a:
        line = line.lower().strip()
        if not line:
            pass
        elif any(c in line for c in "#+-[].,") or "+" in line:
            newcl.append(line)
            continue
        elif line.startswith("def"):
            definition, command, *argnames = line.split()
            if definition == "deffun":
                DEFINITIONS[command] = Definition(line, a)
            elif definition == "defblock":
                DEFINITIONS[command] = BlockDefinition(line, a)
            else:
                debug("ERROR")
                raise
            continue
        else:
            newcl.append(line)

    return newcl


def expandPass(cl):
    a = iter(cl)
    newcl = []

    for line in a:
        line = line.lower().strip()
        if not line:
            continue
        if any(c in line for c in "#+-[].,") or "ptr" in line:
            newcl.append(line)
            continue

        if line.startswith("def"):
            raise

        import copy
        command, *args = line.split()
        newcl.extend(DEFINITIONS[command](*args, iterator=a).strip().split("\n"))
    return newcl


def ptrPass(cl):
    newcl = []
    for line in cl:
        if line.startswith("ptr"):
            command, *args = line.split()
            newcl.extend(*DEFINITIONS[command](*args).strip().split("\n"))
        else:
            newcl.append(line)

    return newcl


if len(sys.argv) < 2:
    sys.argv.append("lib.abf")

c = list(fileinput.input())
oldc = c
c = parseDefs(c)
debug(c)

while True:
    c = expandPass(c)
    debug(c)
    if c == oldc:
        break
    oldc = c



debug(" ".join(c))
c = allocate.allocPass(c)

c = allocate.gotoPass(c)

print(" ".join(c))

debug(allocate)
