#!/usr/bin/env python3
# vim: set fileencoding=utf-8 :

import sys
import functools
import itertools
from fileinput import input
from utils import *
import jump

__appname__     = "assembler"
__author__      = "@AUTHOR@"
__copyright__   = ""
__credits__     = ["@AUTHOR@"]  # Authors and bug reporters
__license__     = "GPL"
__version__     = "1.0"
__maintainers__ = "@AUTHOR@"
__email__       = "@EMAIL@"
__status__      = "Prototype"  # "Prototype", "Development" or "Production"
__module__      = ""


dispatch_map = {}


@functools.partial(dispatch_map.__setitem__, "PTR")
def _(location):
    return ["PTR {}".format(location)]


@functools.partial(dispatch_map.__setitem__, "ZERO")
def _(location):
    return ["PTR {}".format(location), "[", "-",  "]"]


@functools.partial(dispatch_map.__setitem__, "_SET_STATIC")
def _(location, val):
    if val.isdigit():
        set_cmd = ["+"] * int(val)
    else:
        set_cmd = ["_PRIM_SET {}".format(val)]

    return ["ZERO {}".format(location),
            "PTR {}".format(location),
            ] + set_cmd


@functools.partial(dispatch_map.__setitem__, "_PRIM_SET")
def _(val):
    return ["_PRIM_SET {}" .format(val)]


def set_pass(lines):
    for line in lines:
        if line.startswith("_PRIM_SET"):
            command, val = line.split()
            yield from (["+"] * int(val))
        else:
            yield line


def parseLine(line):
    command, *args = line.split()
    if any(c in line for c in "[]+-.,#@"):
        yield line
    elif any(c in line for c in "<>"):
        warn("should really not be using `{}`".format(line))
        yield line
    else:
        for subline in dispatch_map[command](*args):
            if subline == line:
                yield subline
            else:
                yield from parseLine(subline)


def parseLines(lines):
    for line in static_expand_pass(lines):
        yield from parseLine(line)


class Function():
    tmpCounter = strCounter("${:08}TMP")

    def __init__(self, name, argnames, body):
        self.argnames = argnames
        self.name = name
        self.body = body

    def __call__(self, *args):
        assert len(args) == len(self.argnames), (self.name, args, self.argnames)

        retp = []
        for line in self.body:
            outc = line
            for arg, argname in zip(args, self.argnames):
                outc = outc.replace(argname, arg)
            retp.append(outc)

        return retp

    @classmethod
    def parseFunc(cls, line, iterator):
        assert line.startswith('DEFFUN')

        _, command, *argnames = line.split()

        body = []
        for line in iterator:
            if line == 'ENDDEF':
                break

            _, *args2 = line.split()

            newarg = {next(cls.tmpCounter): arg for arg in args2 if arg.startswith("%")}

            mod_line = line
            for fake_arg, real_arg in newarg.items():
                mod_line = mod_line.replace(real_arg, fake_arg)

            ret = list(parseLines([mod_line]))

            for fake_arg, real_arg in newarg.items():
                for index, outline in enumerate(ret):
                    ret[index] = outline.replace(fake_arg, real_arg)

            body.extend(ret)

        return {command: cls(command, argnames, body)}

    @classmethod
    def pass_(cls, iterator, overall_dispatch):
        de_funced = []
        for line in iterator:
            if line.startswith('DEFFUN'):
                overall_dispatch.update(cls.parseFunc(line, iterator))
            else:
                de_funced.append(line)

        return de_funced


class Allocator():
    default_map = {}

    @classmethod
    def pass_(cls, iterator, alloc_map=default_map):
        de_alloced = []
        for line in iterator:
            if not line.startswith('REG'):
                for alias, pointer in alloc_map.items():
                    line = line.replace(alias, str(pointer))
                de_alloced.append(line)
                continue

            _, alias, pointer = line.split()
            if pointer in alloc_map:
                alloc_map[alias] = alloc_map[pointer]
            else:
                if int(pointer) in alloc_map.values():
                    debug(pointer, alias, alloc_map.values())
                    raise
                alloc_map[alias] = int(pointer)

        return de_alloced


def static_expand_pass(lines):
    import re
    for line in lines:
        a = re.findall("@\((\d+)\)", line)
        if a:
            if len(a) > 1:
                warn("uhoh")
            a = int(a[0])
            yield "_SET_STATIC $R4 {}".format(a)
            yield line.replace("@({})".format(a), "$R4")
            continue

        yield line


class Pointerer():
    current_location = 0

    def __init__(self, pos):
        self.pos = int(pos)

    def __call__(self):
        cls = type(self)
        offset = self.pos - cls.current_location

        cls.current_location = self.pos

        if offset > 0:
            return [">"] * offset
        elif offset < 0:
            return ["<"] * abs(offset)

        return []

    @classmethod
    def pass_(cls, iterator):
        ret = []
        for line in iterator:
            if not line.startswith("PTR"):
                ret.append(line)
                continue
            command, *args = line.split()
            ret.extend((cls(args[0])()))
        return ret



def sanitize_one(line):
    line = line.strip()
    if ':' in line:
        line = line[:line.index(':')]
        line = sanitize_one(line)
    return line

def sanitize(iterator):
    ret = []
    for line in iterator:
        line = sanitize_one(line)
        if line:
            ret.append(line)
    return ret


inp = sanitize(input())

inp = Allocator.pass_(iter(inp))

inp = Function.pass_(iter(inp), dispatch_map)

outp = jump.pass_(inp)

outp = parseLines(outp)

outp = Allocator.pass_(outp)

outp = Pointerer.pass_(outp)

outp = set_pass(outp)

print("".join(outp))
