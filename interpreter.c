#include <stdio.h>
#include <unistd.h>

char MEMORY_RAM[10000] = {0};
char COMMAND_RAM[10000] = {0};

int main(int argc, char *argv[]) {
	read(STDIN_FILENO, &COMMAND_RAM, sizeof COMMAND_RAM);

	char *p = MEMORY_RAM;
	char *ip = COMMAND_RAM;



	char *WHILE_PTR = NULL;
	do {
		//printf("%p %c : %p %i\n", ip, *ip, p, *p);
		switch (*ip) {
			case '>': {
				p++;
				continue;
			}
			case '<': {
				p--;
				continue;
			}
			case '+': {
				(*p)++;
				continue;
			}
			case '-': {
				(*p)--;
				continue;
			}
			case '.': {
				putchar(*p);
				continue;
			}
			case '[': {
				WHILE_PTR = ip;
				//printf("hi\n");
				if ((*p) == 0) {
					while (*ip != ']') {
						ip++;
					}
					ip++;
				}
				continue;
			}
			case ']': {
				ip = WHILE_PTR - 1;
				continue;
			}
		}
	} while (*(ip++) != 0);
}
