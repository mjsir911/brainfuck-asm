# Some important instructions

```
PUT $CELL
GET $CELL
```

Output and input to memory respectively

```
SET $CELL $NUM
```

Load immediate `$NUM` into memory location `$CELL`

```
MOV $A $B
COPY $A $B
```

Pretty straight forwards
Note: both squash `$B`
(`MOV` squashes `$B`)

```
ADD $A $B $OUT
SUB $A $B $OUT
```

Add is pretty self explanatory
"Subtract $A from $B" (OUT = B - A)

```
GOTO $ID
ANCHOR $ID
```

yknow... for the program flow

`JMP` is `$JMP` is non-zero

